PetSaverApp (feladat specifikáció)


„Amíg meg nem tapasztaltuk, milyen érzés szeretni egy állatot, lelkünk egy része mélyen alszik.” Szerző: Anatole France



Előzmények:

Egy kisállat mentő alapítvány vezetője Nóra megkeresett minket azzal, hogy készítsünk az alapítvány részére egy webes alkalmazást, ami a napi adminisztrációjukat segíti.

Az alapítvány kisállatok: kutyák, macskák mentésével foglalkozik. 
A mentési munkát és az ideiglenes állattartás feladatait önkéntesek végzik, és amilyen hamar csak lehet, gazdát keresnek nekik.

Jelenleg egy nagyon széles Excel táblába írogatják bele a mentett kisállatok adatait: kutya vagy macska, állapot, súly, kor, szín, fajta. Azt, hogy kapott-e orvosi ellátást, bizonyos oltásokat, és egészségügyi állapotukat. Valamint, hogy melyik menhelyen várja leendő gazdáját, befogadóját. 

Egy igazán lelkes közösség menti a kisállatokat, de sokszor nehézkes az Excel táblába kitölteni az adatokat. Nem tudják melyik rublikába mit írjanak, olykor ki sem töltik, stb. Úgy gondolják, hogy első a mentés és utána vagy sikerül rendesen adminisztrálni vagy nem.

Ugyanakkor a kisállatoknak folyamatosan befogadókat keresnek, akik tudni akarnak sok mindent a kisállatok állapotáról, kinézetéről.

Szoktak nyílt napokat is tartani, ahol személyesen meg lehet nézni a mentett kisállatokat.

Van egy facebook oldaluk is, ahol az érdeklődők megtekinthetik a kisállatok képeit, és az alapítványról szóló híreket.
Nóra szerint a facebook hasznos, de nem alkalmas a mentéssel, tartással, befogadással kapcsolatos adminisztrációk ellátására. 

Ha a mentett kisállataik és adataik egy külön erre a célra kialakított weboldalon megtekinthetőek lennének, akkor hamarabb találnának új gazdára, és a web alkalmazás használatával az alapítvány önkéntesei is könnyebben végezhetnék el az adminisztrációs feladatokat.

Az alapítványnak támogatói is vannak, akik ilyen-olyan összegekkel segítik a menhely működését, illetve az alapítvány pályázható összegekhez is hozzájut. 
Ezeket egy saját számlázó alkalmazással kezelik, ezért a készülő web alkalmazásnak nem célja az alapítvány pénzügyi tranzakcióit kezelni.




Funkcionális követelmények:

•	A weboldalt látogatók végig görgetethessenek egy kép galérián, és egy-egy képre rákattintva a kisállat fontosabb jellemzői legyenek láthatóak.

•	A web alkalmazás kezeljen 3 jogkört: Adminisztrátor, Önkéntes, Látogató. Csak az adminisztrátor és az önkéntesek jelentkezhetnek be a web alkalmazásba. Az önkéntesek a web alkalmazásba regisztrálhassák magukat, amelyet az adminisztrátor véglegesít.

•	Az önkéntesek a kisállatokról képeket tudjanak feltölteni.

•	Az önkéntesek minden kisállatnak egy külön adatlapot is tudjanak nyitni és szerkeszteni, ahol a tulajdonságaikat, egészségügyi eredményeiket, és állapotaikat rögzíteni lehet. Ennek az adatlapnak a fontosabb, publikus elemei, és a kisállat további képei legyenek láthatóak a weboldalt látogatók számára.

•	Legyen a látogatok számára egy gyors bejelentő felület, ahol akár egy látogatótól elveszett kisállat, akár egy látogató által talált kisállat jelenthető lenne, és az önkéntesek erről értesítést (email) kapnának.

•	A bejelentő felület egy űrlap kitöltéséből álljon, ahol a bejelentő megadhatja az elérhetőségeit (email, telefon), a kisállat nevét, jellemzőit, és fotót is feltölthetne. Talált vagy látott kisállat esetén meg szintén feltölthetne fotót és helyszín adatokat, illetve a saját elérhetőségeit.

•	A weboldalon elszeparáltan egymástól jól megkülönböztethetően legyenek láthatóak a mentett, megtalált kisállatok, és az elveszett kisállatok képei, adatlapjai.

•	A weboldalon a látogatók érdeklődésüket fejezhessék ki egy mentett, gazdi nélküli kisállat iránt, a kisállat adatlapján egy mini űrlapot kitöltve az elérhetőségeikkel. 

•	Befogadás esetén további adatokat adhassanak meg magukról, amelyek kerüljenek rá a kisállatok adat lapjaira.




A fejlesztés során használandó technológiák:

•	Adatbázis: MySQL.

•	Java Dependencies: Maven.

•	Java perzisztens réteg: JPA - Hibernate.

•	Alkalmazás szerver és security: Payara Security Realm

•	Kontroll és Logikai réteg: Java Servletek, Enterprise Java Bean-ek (EJB).

•	View réteg: JSP és HTML oldalak, JSTL technológia, BootStrap.



Tovább fejlesztési lehetőségek:

Lehetne egy közös cset felület az önkéntesek számára, hogy élő csetben riaszthassák egymást, ha valaki kóborló kisállatot lát, 
illetve egyéb dolgokról diskurálhassanak munka vagy mentés közben.




