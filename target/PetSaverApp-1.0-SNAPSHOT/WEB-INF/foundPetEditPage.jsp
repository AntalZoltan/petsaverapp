<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>foundPetEditPagee</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
            body, html {height: 100%}
            body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
            
            .updateFoundPetPage{
            background-color: #F5F5DC;
        }
        
        </style>
 
    </head>
    <body class="updateFoundPetPage">
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>
                
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>
                
                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-3"></div>
                <div class="w3-container rapid w3-padding-20 w3-card">
                    <p style="font-size:2vw; color:white" >Megtalált</p>
                    <h1>Megtalált kisállat adatlap szerkesztése másodszorra</h1>
                    <c:if test="${not empty foundPet}">
                    <h2>Gyors bejelentőtől kapott adatok:</h2>
                    
                    <div>
                        <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Vissza a listákhoz</a>
                    </div>        
                        </br>
                                                        
                    <div id="foundForm">
                        <form  action="foundPetEditServlet" method="post">
                            <label for="nickName">Kisállat neve:
                                <input id="nickName" name="nickName" class="textfield" type="text" value="${foundPet.nickName}">
                            </label>
                            <br/>
                            <label for="catOrDog">Kutya vagy macska?
                            <select name="catOrDog">
                                <option value="TRUE" ${foundPet.catOrDog eq 'true' ? 'selected' : ''}>kutya</option>
                                <option value="FALSE" ${foundPet.catOrDog eq 'false' ? 'selected' : ''}>macska</option>                              
                            </select>
                            </label>
                            <br/>
                            <label for="age">Kor:
                                <input id="age" name="age" class="textfield" type="text" value="${foundPet.age}">
                            </label>
                            <br/>                          
                            <label for="color">Szín:
                                <input id="color" name="color" class="textfield" type="text" value="${foundPet.color}">
                            </label>
                            <br/>
                            <div class="form-group">
                                Állat neme:<br>
                                <label class="container">kan
                                    <input type="radio" checked="checked" value="TRUE" name="sex" id="sex" checked="${foundPet.sex=='male'?'checked':''}">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container">szuka
                                    <input type="radio" checked="checked" value="TRUE" name="sex" id="sex" ${foundPet.sex=='female'?'checked':''}>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <br/>
                            <label for="weight">Súly:
                                <input id="weight" name="weight" class="textfield" type="text" value="${foundPet.weight}">
                            </label>
                            <br/>
                            <label for="type">type: 
                                <input id="subtype" name="type" class="textfield" type="text" value="${foundPet.type}">
                            </label>
                            <br/>
                            <label for="subType">subtype: 
                                <input id="subType" name="subType" class="textfield" type="text" value="${foundPet.subType}">
                            </label>
                            <br/>
                            <label for="dangerous">Veszélyes?
                            <select name="dangerous">
                                <option value="FALSE" ${foundPet.dangerous eq 'false' ? 'selected' : ''}>nem</option>
                                <option value="TRUE" ${foundPet.dangerous eq 'true' ? 'selected' : ''}>igen</option>                                
                            </select>
                            </label>
                            <br/>
                            <label for="image">Kép:
                                <image id="image" name="image" src="DisplayImageServlet?name=${foundPet.image}" height="100" width="100">
                            </label>
                            <br/>
                            <input type="hidden" id="imageURL" name="imageURL" value="${foundPet.image}">
                            <br/>
                           <c:if test="${not empty rescues}">                               
                                <table id="rescue" border="solid 1px">                                   
                                    <thead>
                                    <th>Megtalálás ideje</th>
                                    <th>Megtalálás helye</th>
                                    <th>Megtalálás körülményei</th>                     
                                    <th>Megtaláló</th>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${rescues}" var="item">
                                        <tr>
                                            <td>${item.rescueDate}</td>
                                            <td>${item.rescuePlace}</td>                          
                                            <td>${item.rescueNote}</td>
                                            <td>${foundPet.getAnnouncer().getEmailOfAnnouncer()}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>                                   
                                </table>                                  
                                <label for="rescuePlace">Megtalálás helye:
                                    <input id="rescuePlace" name="rescuePlace" class="textfield" type="text">
                                </label>
                                <br/>
                                <label for="rescueDate">Ekkor találták meg:
                                    <input id="rescueDate" name="rescueDate" type="date">
                                </label>
                                <br/>
                                <label for="rescueNote">Megtalálás körülményei:
                                    <input id="rescueNote" name="rescueNote" class="textfield" type="text">
                                </label>
                                <br/>
                                <input type="hidden" id="rescues" name="rescues" value="${rescues}">
                                <br/>
                                <input type="hidden" id="announcerId" name="announcerId" value="${Long.valueOf(foundPet.announcer.getAnnouncerId())}">
                                <br/>
                            <br/>
                             </c:if>
                            
                             <c:if test="${not empty chips}">
                                  <table id="rescue" border="solid 1px">                                   
                                    <thead>
                                    <th>Chippelés dátuma</th>
                                    <th>Chip szám</th>
                                    <th>Állatorvos neve</th>                     
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${chips}" var="item">
                                        <tr>
                                            <td>${item.chipDate}</td>                          
                                            <td>${item.chipNumber}</td>
                                            <td>${item.vetName}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>                                   
                                </table> 
                                <label for="chipNumber">Chip szám:
                                    <input id="chipNumber" name="chipNumber" class="textfield" type="text">
                                </label>
                                <br/>
                                <label for="chipDate">Chipelés dátuma:
                                    <input id="chipDate" name="chipDate" class="textfield" type="date">
                                </label>
                                <br/>
                                <label for="vetNameChip">Álatorvos neve:
                                    <input id="vetNameChip" name="vetNameChip" class="textfield" type="text">
                                </label>
                                <br/>
                                <input type="hidden" id="chips" name="chips" value="${chips}">
                                <br/>
                             </c:if>
                            
                            <c:if test="${not empty diagnoses}">
                                <table id="rescue" border="solid 1px">                                   
                                    <thead>
                                    <th>Diagnozis dátuma</th>
                                    <th>Oka</th>
                                    <th>Eredménye</th>                     
                                    <th>Szükséges gyógyszer</th>
                                    <th>Állatorvos neve</th>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${diagnoses}" var="item">
                                        <tr>
                                            <td>${item.diagDate}</td>                          
                                            <td>${item.cause}</td>
                                            <td>${item.result}</td>
                                            <td>${item.medicine}</td>
                                            <td>${item.vetName}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>                                   
                                </table> 
                                <label for="diagDate">Egészségi felmérés dátuma:
                                    <input id="diagDate" name="diagDate" class="textfield" type="date">
                                </label>
                                <br/>
                                <label for="cause">Egészségi felmérés oka:
                                    <input id="cause" name="cause" class="textfield" type="text">
                                </label>
                                <br/>  
                                <label for="result">Egészségi felmérés eredménye:
                                    <input id="result" name="result" class="textfield" type="text">
                                </label>
                                <br/>
                                <label for="medicine">Szükséges gyógyszer:
                                    <input id="medicine" name="medicine" class="textfield" type="text">
                                </label>
                                <br/>
                                <label for="vetNameDiag">Álatorvos neve:
                                    <input id="vetNameDiag" name="vetNameDiag" class="textfield" type="text">
                                </label>
                                <br/>
                                <input type="hidden" id="diagnoses" name="diagnoses" value="${diagnoses}">
                                <br/>
                            </c:if>
                            
                            <c:if test="${not empty vaccinations}">
                                <table id="rescue" border="solid 1px">                                   
                                    <thead>
                                    <th>Oltás dátuma</th>
                                    <th>Oltás tipusa</th>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${vaccinations}" var="item">
                                        <tr>
                                            <td>${item.vaccDate}</td>                          
                                            <td>${item.vaccType}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>                                   
                                </table> 
                                <label for="vaccinationDate">Oltás dátuma:
                                    <input id="vaccinationDate" name="vaccinationDate" class="textfield" type="date">
                                </label>
                                <br/>
                                <label for="vaccinationType">Oltás tipusa:
                                    <input id="vaccinationType" name="vaccinationType" class="textfield" type="text">
                                </label>
                                <br/>
                                <input type="hidden" id="vaccinations" name="vaccinations" value="${vaccinations}">
                                <br/>
                            </c:if>
                            
                            <input type="hidden" id="fosterid" name="fosterid" value="">
                            <br/>
                            <label for="userEmail">Felelős önkéntes:
                            <select name="userEmail">
                                <option value="orsi@gmail.com" ${foundPet.getUser().getUserEmail() eq 'orsi@gmail.com' ? 'selected' : ''}>orsi@gmail.com</option>
                                <option value="jani@gmail.com" ${foundPet.getUser().getUserEmail() eq 'jani@gmail.com' ? 'selected' : ''}>jani@gmail.com</option>
                                <option value="zoli@gmail.com" ${foundPet.getUser().getUserEmail() eq 'zoli@gmail.com' ? 'selected' : ''}>zoli@gmail.com</option>
                                <option value="user@gmail.com" ${foundPet.getUser().getUserEmail() eq 'user@gmail.com' ? 'selected' : ''}>user@gmail.com</option>
                            </select>
                            </label>
                            <br/>
                            <input type="hidden" id="petId" name="petId" value="${foundPet.id}">                           
                            <br/>
                            <button type="submit">Adatlap mentése</button>
                            <input type="button" onclick="location.href='foundPageServlet';" value="Vissza" />
      
                        </form>
                    </div>
                    </c:if>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </body>
</html>
