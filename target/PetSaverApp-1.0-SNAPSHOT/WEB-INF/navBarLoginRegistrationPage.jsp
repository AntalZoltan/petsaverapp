<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <title>PetSaverApp</title>-->
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        
        <style>
            body, html {height: 100%}
            body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
        </style>
        <title>
            
        </title>
        <body>
        <div id="topmenu_navbar">
       
        <c:if test="${pageContext.request.getUserPrincipal() == null}">
            <a style="float:right" href="registrationServlet" class="active w3-bar-item w3-button w3-padding-large w3-hide-small">Regisztráció</a>

            <a style="float:right" href="loginServlet" class="active w3-bar-item w3-button w3-padding-large w3-hide-small">Bejelentkezés</a>
         </c:if>
         <c:if test="${pageContext.request.getUserPrincipal() != null}">
              Üdvözlünk kedves: ${pageContext.request.getUserPrincipal()}
            <a style="float:right" href="logoutServlet" class="active w3-bar-item w3-button w3-padding-large w3-hide-small"> Kijelentkezés</a>
        </c:if>
        </div>
    </body>
</html>
