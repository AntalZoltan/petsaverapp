<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    </head>
    <style>
        body, html {height: 100%}
        body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}

        .updateTry{
            background-color: #F5F5DC;
        }
        
    </style>

    <body class="updateTry">
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Örökbefogadható</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="w3-container rapid w3-padding-20 w3-card">
                    <p style="font-size:2vw; color:white" >Megtalált</p>
                    <h1>Hello World!</h1>
                    <h1>Megtalált kisállat adatlap szerkesztése:</h1>
                    <h2>Gyors bejelentőtől kapott adatok:</h2>
                    <table id="announced">
                        <th>Megtalált hely</th>
                        <th>Megtalált időpont</th>
                        <th>Kisállat tulajdonságai</th>                      
                        <th>Megtaláló</th>
                        <tr>
                            <td>${pet.foundPlace}</td>                          
                            <td>${pet.foundDate}</td>
                            <td>${pet.petProperties}</td>
                            <td>${pet.announcer.getEmailOfAnnouncer()}</td>
                        </tr>
                    </table>

                    </br>
                    <div id="foundForm">
                        <form  action="foundPageServlet" method="post">
                            <label for="petName">Kisállat neve:
                                <input id="petName" name="petName" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="age">Kisállat kora:
                                <input id="age" name="age" class="textfield" type="text">
                            </label>
                            <br/>
                            <select name="catordog" form="kutyav">
                                <option value="" selected disabled hidden>Kutya vagy macska?</option>
                                <option>macska</option>
                                <option>kutya</option>
                            </select>
                            <br/>
                            <label for="color">Szín:
                                <input id="color" name="color" class="textfield" type="text">
                            </label>
                            <br/>
                            <select name="dangerous">
                                <option value="" selected disabled hidden>Veszélyes?</option>
                                <option>igen</option>
                                <option>nem</option>
                            </select>
                            <br/>
                            <label for="sex">Neme:
                                <input id="sex" name="sex" class="textfield" type="text" value="${pet.sex}">
                            </label>
                            <br/>
                            <label for="petProperties">Egyéb tudnivalók:
                                <input id="petProperties" name="petProperties" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="image">Képlink:
                                <input id="image" name="image" class="textfield" type="text" value="${pet.image}">
                            </label>
                            <br/>
                            <label for="owned">Volt már örökbefogadva: 
                                <input id="owned" name="owned" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="subtype">subtype: 
                                <input id="subtype" name="subtype" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="type:">type: 
                                <input id="subtype" name="type:" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="weight">Súly:
                                <input id="weight" name="weight" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="fosterid">Örökbefogadó:
                                <input id="fosterid" name="fosterid" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="userid">userid
                                <input id="userid" name="userid" class="textfield" type="text">
                            </label>
                            <br/>
                            <button type="submit">Adatlap mentése</button>
                            <input type="button" onclick="location.href = 'userPageServlet';" value="Vissza" />

                        </form>
                    </div>                                    

                </div>
            </div>
        </div>
    </body>
</html>
