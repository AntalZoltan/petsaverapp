package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.HealthDiagnoseEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class HealthDiagnoseDAO extends BaseDAO{
    
    public List<HealthDiagnoseEntity> getDiagnosesById(Long petId){
        return em.createNamedQuery(HealthDiagnoseEntity.NQ_GET_DIAGNOSES_BY_ID, HealthDiagnoseEntity.class)
                .setParameter(HealthDiagnoseEntity.PARAM_PET_ID, petId)
                .getResultList();
    }
    
    @Transactional
    public void updateEntity(HealthDiagnoseEntity diagEntity){
        em.merge(diagEntity);
    }
    
}
