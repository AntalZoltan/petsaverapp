package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.VaccinationEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class VaccinationDAO extends BaseDAO{
    
    public List<VaccinationEntity> getVaccinationsById(Long petId){
        return em.createNamedQuery(VaccinationEntity.NQ_GET_VACCINATIONS_BY_ID, VaccinationEntity.class)
                .setParameter(VaccinationEntity.PARAM_PET_ID, petId)
                .getResultList();
    }
    
    @Transactional
    public void updateEntity(VaccinationEntity vaccEntity){
        em.merge(vaccEntity);
    }
    
}
