package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.ChipEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class ChipDAO extends BaseDAO {
    
    public List<ChipEntity> getChipsById(Long petId){
        return em.createNamedQuery(ChipEntity.NQ_GET_CHIPS_BY_ID, ChipEntity.class)
                .setParameter(ChipEntity.PARAM_PET_ID, petId)
                .getResultList();
    }
    
    public ChipEntity getChipById(Long chipId){
        return em.createNamedQuery(ChipEntity.NQ_GET_CHIP_BY_ID, ChipEntity.class)
                .setParameter(ChipEntity.PARAM_CHIP_ID, chipId)
                .getSingleResult();
    }
    
    @Transactional
    public void updateEntity(ChipEntity chipEntity){
        em.merge(chipEntity);
    }
    
}
