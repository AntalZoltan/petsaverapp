package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.BaseEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class BaseDAO<T extends BaseEntity> {
    
    @PersistenceContext(unitName = "PetSaverAppPU")
    protected EntityManager em;

    @Transactional
    public void createEntity(T entity)
    {
        em.persist(entity);
    }
    
    @Transactional(Transactional.TxType.SUPPORTS)
    public List<T> findAll(Class<T> clazz)
    {
        return em.createQuery("select w from "
                + clazz.getSimpleName() + " w", clazz).getResultList();
    }
}
