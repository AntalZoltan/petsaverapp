package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;

public class FoundDTO {
    
    private Long id;
    private String foundPlace;   
    private String sex;  
    private String petProperties;   
    private LocalDate foundDate;
    private String image;
    private AnnouncerDTO announcer;
    private boolean added;

    public FoundDTO() {
    }

   

    public FoundDTO(String foundPlace, String sex, String petProperties, LocalDate foundDate, String image, AnnouncerDTO announcer, boolean added) {
        this.foundPlace = foundPlace;
        this.sex = sex;
        this.petProperties = petProperties;
        this.foundDate = foundDate;
        this.image = image;
        this.announcer = announcer;
        this.added = added;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    public String getFoundPlace() {
        return foundPlace;
    }

    public void setFoundPlace(String foundPlace) {
        this.foundPlace = foundPlace;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPetProperties() {
        return petProperties;
    }

    public void setPetProperties(String petProperties) {
        this.petProperties = petProperties;
    }

    public LocalDate getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(LocalDate foundDate) {
        this.foundDate = foundDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public AnnouncerDTO getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(AnnouncerDTO announcer) {
        this.announcer = announcer;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    @Override
    public String toString() {
        return "FoundDTO{" + "id=" + id + ", foundPlace=" + foundPlace + ", sex=" + sex + ", petProperties=" + petProperties + ", foundDate=" + foundDate + ", image=" + image + ", announcer=" + announcer + ", added=" + added + '}';
    }
    
    
}
