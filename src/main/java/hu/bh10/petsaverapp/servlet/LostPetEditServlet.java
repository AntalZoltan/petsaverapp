package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.ChipDTO;
import hu.bh10.petsaverapp.dto.HealthDiagnoseDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.dto.VaccinationDTO;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.ChipService;
import hu.bh10.petsaverapp.service.HealthDiagnoseService;
import hu.bh10.petsaverapp.service.PetService;
import hu.bh10.petsaverapp.service.RescueCircumstanceService;
import hu.bh10.petsaverapp.service.VaccinationService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LostPetEditServlet", urlPatterns = {"/lostPetEditServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class LostPetEditServlet extends HttpServlet {

    @Inject
    private PetService petService;
    
    @Inject
    private RescueCircumstanceService rescueService;
    
    @Inject
    private HealthDiagnoseService healthService;
    
    @Inject
    private ChipService chipService;
    
    @Inject
    private VaccinationService vaccinationService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        Long petId = Long.valueOf(request.getParameter("petId"));
        PetDTO lostPetDTO = PetMapper.toPetDTO(petService.getPetById(petId));
        
        //El kell kérni a DB-ből a pet-hez tartozó megtalálási körülményket.
        List<RescueCircumstanceDTO> rescues = rescueService.getRescuesById(petId);
        
        //El kell kérni a DB-ből a pet-hez tartozó chip számot és chippelés körülményket.
        List<ChipDTO> chips = chipService.getChipsById(petId);
        
        //El kell kérni a DB-ből a pet-hez tartozó diagnosztikákat.
        List<HealthDiagnoseDTO> diagnoses = healthService.getDiagnosesById(petId);
        
        //El kell kérni a DB-ből a pet-hez tartozó oltásokat.
        List<VaccinationDTO> vaccinations = vaccinationService.getVaccinationsById(petId);
        
        
        session.setAttribute("lostPet", lostPetDTO);
        session.setAttribute("rescues", rescues);
        session.setAttribute("chips", chips);
        session.setAttribute("diagnoses", diagnoses);
        session.setAttribute("vaccinations", vaccinations);
        
        session.setAttribute("lostPet", lostPetDTO);
        request.getRequestDispatcher("WEB-INF/lostPetEditPage.jsp").forward(request, response);
        
         
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        
        Long petId = Long.valueOf(request.getParameter("petId"));
        String isFound = "false";
        
        String nickName = request.getParameter("nickName");
        String catOrDog = request.getParameter("catOrDog");
        String age = request.getParameter("age");
        String color = request.getParameter("color");
        String sex = request.getParameter("sex");
        String weight = request.getParameter("weight");
        String type = request.getParameter("type");
        String subType = request.getParameter("subType");
        String dangerous = request.getParameter("dangerous");
        String image = (String)request.getParameter("imageURL");
        
        
        //Rescue-megtalálás esetleges módositása
        String rescueDate = request.getParameter("rescueDate");
        String rescuePlace = request.getParameter("rescuePlace");
        String rescueNote = request.getParameter("rescueNote");
        List<RescueCircumstanceDTO> rescues = (List<RescueCircumstanceDTO>) session.getAttribute("rescues");
        Long rescueId = 0L;
        for (int i = 0; i < rescues.size(); i++) {
            rescueId = rescues.get(i).getId();
        }
        rescueService.updateRescueCircumstance(rescueDate, rescuePlace, rescueNote, petId, rescueId);
        
        //Chip-elés esetlegs módositása
        String chipDate = request.getParameter("chipDate");
        String chipNumber = request.getParameter("chipNumber");       
        String vetNameChip = request.getParameter("vetNameChip");
        List<ChipDTO> chips = (List<ChipDTO>) session.getAttribute("chips");
        Long chipId = 0L;
        for (int i = 0; i < chips.size(); i++) {
            chipId = chips.get(i).getId();
        }
        chipService.updateChip(chipDate, chipNumber, vetNameChip, petId, chipId);
        
        //Felelős önkéntes esetleges módositása
        String userEmail = request.getParameter("userEmail");
        
        Long announcerId = Long.valueOf(request.getParameter("announcerId"));
        
        petService.updatePet(nickName, catOrDog, age, color, sex, weight, type, subType, dangerous, isFound ,image, userEmail, petId);

        response.sendRedirect("lostPageServlet");
    }
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
