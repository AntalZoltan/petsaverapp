package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.ChipDTO;
import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.dto.HealthDiagnoseDTO;
import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.dto.VaccinationDTO;
import hu.bh10.petsaverapp.entity.FoundEntity;
import hu.bh10.petsaverapp.entity.LostEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.mapper.AnnouncerMapper;
import hu.bh10.petsaverapp.mapper.FoundMapper;
import hu.bh10.petsaverapp.mapper.LostMapper;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.AnnouncerService;
import hu.bh10.petsaverapp.service.ChipService;
import hu.bh10.petsaverapp.service.FosterService;
import hu.bh10.petsaverapp.service.HealthDiagnoseService;
import hu.bh10.petsaverapp.service.LostFormService;
import hu.bh10.petsaverapp.service.PetService;
import hu.bh10.petsaverapp.service.RescueCircumstanceService;
import hu.bh10.petsaverapp.service.UserService;
import hu.bh10.petsaverapp.service.VaccinationService;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/lostPageServlet"})
@DeclareRoles({"admin", "user"})
public class LostPageServlet extends HttpServlet {
    
    @Inject
    private UserService userService;
    
    @Inject
    private AnnouncerService announcerService;
    
    @Inject
    private FosterService fosterService;
    
    @Inject
    private PetService petService;
    
    @Inject
    private RescueCircumstanceService rescueCircumstanceService;
    
    @Inject
    private ChipService chipService;
    
    @Inject
    private HealthDiagnoseService healthDiagnoseService;
    
    @Inject
    private VaccinationService vaccinationService;
    
    @Inject
    private LostFormService lostFormService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Itt kell a DB-ből kivenni a rekordokat és feltölteni egy listát
        HttpSession session = request.getSession();
        List<PetEntity> entities = petService.findPets(false);//false = lostPets
        List<PetDTO> lostPetsList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            PetDTO pet = PetMapper.toPetDTO(entities.get(i));
            lostPetsList.add(pet);
        }
        session.setAttribute("lostPetsList", lostPetsList);
        request.getRequestDispatcher("WEB-INF/lostPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setCharacterEncoding("UTF-8");
        
        //Itt elkérni az adatokat az updateFoundPetPage.jsp-től.
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        
        String nickName = (String)request.getParameter("nickName");//Kötelező
        String catOrDog = (String)request.getParameter("catOrDog");//Kötelező
        String age = (String)request.getParameter("age");//Kötelező
        String color = (String)request.getParameter("color");//Kötelező
        String sex = (String)request.getParameter("sex");//Kötelező
        String weight = (String)request.getParameter("weight");
        String type = (String)request.getParameter("type");
        String subType = (String)request.getParameter("subType");
        String dangerous = request.getParameter("dangerous");
        String imageURL = (String)request.getParameter("image");//Kötelező
        
        //Összeállitjuk a PetDTO-t
        PetDTO freshPet = new PetDTO();
        freshPet.setNickName(nickName);
        if(catOrDog.toUpperCase().equals("TRUE")){
            freshPet.setCatOrDog(true);
        }else{
            freshPet.setCatOrDog(false);
        }
        freshPet.setAge(Integer.valueOf(age));
        freshPet.setColor(color);
        if(sex.toUpperCase().equals("TRUE")){
            freshPet.setSex(true);
        }else{
            freshPet.setSex(false);
        }
        if(weight!=null  && !weight.isEmpty()){
            freshPet.setWeight(Double.valueOf(weight));
        }
        freshPet.setType(type);
        freshPet.setSubType(subType);
        if(dangerous.toUpperCase().equals("TRUE")){
            freshPet.setDangerous(true);
        }else{
            freshPet.setDangerous(false);
        }
        freshPet.setImage(imageURL);
        freshPet.setFound(false);//Mivel elveszett kisállat adatain dolgozunk ezért ennek értéke false.
        freshPet.setInterestCounter(0);//Ezt kezdetben 0-r állitjuk.
        freshPet.setSuccess(false);//Ezt kezdetben false-ra állitjuk.

        //Megszerezzük a User objektumot
        String userEmail = (String)request.getParameter("userEmail");//Kötelező       
        UserDTO userDTO = userService.getUserByEmail(userEmail);
        freshPet.setUser(userDTO);
        
        //Megszerezzök az Announcer objektumot
        String announcerid = (String)request.getParameter("announcerid");//Kötelező
        Long announcerId = Long.valueOf(announcerid);
        AnnouncerDTO announcerDTO = announcerService.getAnnouncerById(announcerId);
        System.out.println("Announcer: "+announcerDTO);
        freshPet.setAnnouncer(announcerDTO);
        
        //Itt elmentjük a PetDTO-t a DB-be.
        petService.createPet(freshPet, announcerId, userEmail);
        
        
        //Itt vissza kell kérni a frissen mentett objektum id-ját, hogy az id alapján elkért Pet objektumot beletehessük a 4 db mellék táblába.
        Long petId = petService.getMaxPetId();
        
        PetEntity savedPet = petService.getPetById(petId);
        PetDTO savedPetDTO = PetMapper.toPetDTO(savedPet);
        
        //RescueCircumstance objektum létrehozása a Pet objektummal.
        String lastKnownPlace = (String)request.getParameter("lastKnownPlace");//Kötelező
        String lostDateString = request.getParameter("lostDate");//Kötelező       
        String lostNote = (String)request.getParameter("lostNote");
        if((lostDateString!=null && !lostDateString.isEmpty()) && (lastKnownPlace!=null && !lastKnownPlace.isEmpty())){
            LocalDate lostDate = LocalDate.parse(lostDateString, dtf);
            RescueCircumstanceDTO rescueDTO = new RescueCircumstanceDTO();
            rescueDTO.setRescueDate(lostDate);
            rescueDTO.setRescuePlace(lastKnownPlace);
            rescueDTO.setRescueNote(lostNote);
            rescueCircumstanceService.createRescueCircumstance(rescueDTO, petId);
        }

        //Chip objektum létrehozása a Pet objektummal.
        String chipNumber = (String)request.getParameter("chipNumber");
        String chipDateString = request.getParameter("chipDate");
        if((chipDateString!=null && !chipDateString.isEmpty()) && (chipNumber!=null  && !chipNumber.isEmpty())){
            LocalDate chipDate = LocalDate.parse(chipDateString, dtf);
            ChipDTO chipDTO = new ChipDTO();
            chipDTO.setChipNumber(chipNumber);
            chipDTO.setChipDate(chipDate);
            chipService.createChip(chipDTO, petId);
        }
           
        //Itt vesszük le a user page gyorsbejelentő listáról a bejelentett elveszett kisállatot.(Az added értékét true-ra állitjuk)
        String lostPetId = (String)request.getParameter("petId");
        lostFormService.updateLost(Long.valueOf(lostPetId));
        
        //Itt kell a DB-ből kivenni a rekordokat és feltölteni egy listát
        HttpSession session = request.getSession();
        List<PetEntity> entities = petService.findPets(savedPetDTO.isFound());
        List<PetDTO> lostPetsList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            PetDTO pet = PetMapper.toPetDTO(entities.get(i));
            lostPetsList.add(pet);
        }
        session.setAttribute("lostPetsList", lostPetsList);

        //Itt kell átküldeni a listát a lostPage.jsp-nek.
        System.out.println("Kisállat neve: "+nickName);
        request.getRequestDispatcher("WEB-INF/lostPage.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
