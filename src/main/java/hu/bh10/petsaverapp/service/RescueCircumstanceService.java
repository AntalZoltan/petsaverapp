package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dao.RescueCircumstanceDAO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.RescueCircumstanceEntity;
import hu.bh10.petsaverapp.mapper.RescueCircumstanceMapper;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class RescueCircumstanceService {
    
    @Inject
    RescueCircumstanceDAO rescueDAO;
    
    @Inject
    PetDAO petDAO;
    
    public void createRescueCircumstance(RescueCircumstanceDTO rescueCircumstance, Long petId) {
        PetEntity petEntity = petDAO.getPetById(petId);
        RescueCircumstanceEntity entity = RescueCircumstanceMapper.toEntity(rescueCircumstance, petEntity);
        rescueDAO.createEntity(entity);
    }
    
    public List<RescueCircumstanceDTO> getRescuesById(Long petId){
        List<RescueCircumstanceDTO> rescues = new ArrayList<>();
        rescues = RescueCircumstanceMapper.toRescueDTOList(rescueDAO.getRescuesById(petId));
        return rescues;
    }
    
    public void updateRescueCircumstance(String rescueDateString, String rescuePlace, String rescueNote, Long petId, Long rescueId) {
        if((rescueDateString!=null && !rescueDateString.isEmpty()) && (rescuePlace!=null && !rescuePlace.isEmpty()) && (rescueNote!=null && !rescueNote.isEmpty())){
            RescueCircumstanceEntity entity = rescueDAO.getRescueById(rescueId);
            Date rescueDate = Date.valueOf(rescueDateString);
            entity.setRescueDate(rescueDate);
            entity.setRescuePlace(rescuePlace);
            entity.setRescueNote(rescueNote);
            rescueDAO.updateEntity(entity);
        }
        
    }
}
