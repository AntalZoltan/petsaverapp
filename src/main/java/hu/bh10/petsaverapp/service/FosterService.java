package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.FosterDAO;
import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.entity.FosterEntity;
import hu.bh10.petsaverapp.mapper.FosterMapper;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class FosterService {

    @Inject
    FosterDAO fosterDAO;

    public void createFoster(FosterDTO foster) {
            FosterEntity entity = FosterMapper.toEntity(foster);            
            fosterDAO.createEntity(entity);
    }
    
    public boolean fosterIsExist(String fosterEmail){
        return fosterDAO.fosterIsExist(fosterEmail);
    }
    
    public FosterDTO getFosterByEmail(String fosterEmail){
        FosterDTO foster = FosterMapper.toDTO(fosterDAO.getFosterByEmail(fosterEmail));
        return foster;
    }

}
