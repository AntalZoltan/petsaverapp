package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.UserGroupDTO;
import hu.bh10.petsaverapp.entity.UserGroup;
import java.util.ArrayList;
import java.util.List;

public class UserGroupMapper {
    
    public static UserGroup toUserGroupEntity(UserGroupDTO dto){
        UserGroup entity = new UserGroup();
        entity.setGroupName(dto.getGroupName());
        entity.setUserGroups(UserMapper.toUserEntityList(dto.getUserGroups()));
        
        return entity;
    }
    
    public static UserGroupDTO toUserGroupDTO(UserGroup entity){
        UserGroupDTO dto = new UserGroupDTO();
        dto.setGroupName(entity.getGroupName());
        dto.setUserGroups(UserMapper.toUserDTOList(entity.getUserGroups()));
        
        return dto;
    }
    
    public static List<UserGroup> toUserGroupEntityList(List<UserGroupDTO> dtoList){
        List<UserGroup> entityList = new ArrayList<>();
        for (int i = 0; i < dtoList.size(); i++) {
            UserGroup entity = UserGroupMapper.toUserGroupEntity(dtoList.get(i));
            entityList.add(entity);
        }
        return entityList;
    }
    
    public static List<UserGroupDTO> toUserGroupDTOList(List<UserGroup> entityList){
        List<UserGroupDTO> dtoList = new ArrayList<>();
        for (int i = 0; i < entityList.size(); i++) {
            UserGroupDTO dto = UserGroupMapper.toUserGroupDTO(entityList.get(i));
            dtoList.add(dto);
        }
        return dtoList;
    }
    
}
