package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.FosterEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.UserEntity;
import java.util.ArrayList;
import java.util.List;

public class PetMapper {

    public PetMapper() {
    }
    
    public static PetEntity toEntity(PetDTO petDTO, AnnouncerEntity announcerEntity, UserEntity userEntity, FosterEntity fosterEntity){
        PetEntity entity = new PetEntity();
        entity.setNickName(petDTO.getNickName());
        if(petDTO.isCatOrDog()){
            entity.setCatOrDog("true");
        }else{
            entity.setCatOrDog("false");
        }
        entity.setAge(petDTO.getAge());
        entity.setColor(petDTO.getColor());
        if(petDTO.isSex()){
            entity.setSex("true");
        }else{
            entity.setSex("false");
        }
        entity.setWeight(petDTO.getWeight());
        entity.setType(petDTO.getType());
        entity.setSubtype(petDTO.getSubType());
        if(petDTO.isOwned()){
            entity.setOwned("true");
        }else{
            entity.setOwned("false");
        }
        if(petDTO.isDangerous()){
            entity.setDangerous("true");
        }else{
            entity.setDangerous("false");
        }
        if(petDTO.isFound()){
            entity.setFound("true");
        }else{
            entity.setFound("false");
        }
        entity.setImage(petDTO.getImage());
        entity.setInterestCounter(petDTO.getInterestCounter());
        if(petDTO.isSuccess()){
            entity.setSuccess("true");
        }else{
            entity.setSuccess("false");
        }
        entity.setHandlerAnnouncer(announcerEntity);
        entity.setHandlerUser(userEntity);
        entity.setHandlerFoster(fosterEntity);
        return entity;
 
    }
    
    public static PetEntity toPetEntity(PetDTO dto){
        PetEntity entity = new PetEntity();
        entity.setNickName(dto.getNickName());
        if(dto.isCatOrDog()){
            entity.setCatOrDog("true");
        }else{
            entity.setCatOrDog("false");
        }
        entity.setAge(dto.getAge());
        entity.setColor(dto.getColor());
        if(dto.isSex()){
            entity.setSex("true");
        }else{
            entity.setSex("false");
        }
        entity.setWeight(dto.getWeight());
        entity.setType(dto.getType());
        entity.setSubtype(dto.getSubType());
        if(dto.isOwned()){
            entity.setOwned("true");
        }else{
            entity.setOwned("false");
        }
        if(dto.isDangerous()){
            entity.setDangerous("true");
        }else{
            entity.setDangerous("false");
        }
        if(dto.isFound()){
            entity.setFound("true");
        }else{
            entity.setFound("false");
        }
        entity.setImage(dto.getImage());
        entity.setInterestCounter(dto.getInterestCounter());
        if(dto.isSuccess()){
            entity.setSuccess("true");
        }else{
            entity.setSuccess("false");
        }
        entity.setHandlerAnnouncer(AnnouncerMapper.toAnnouncerEntity(dto.getAnnouncer()));
        entity.setHandlerUser(UserMapper.toUserEntity(dto.getUser()));
        entity.setHandlerFoster(FosterMapper.toEntity(dto.getFoster()));
        
        return entity;
    }
    
    public static PetDTO toPetDTO(PetEntity petEntity){
        PetDTO dto = new PetDTO();
        dto.setId(petEntity.getId());
        dto.setNickName(petEntity.getNickName());
        if(petEntity.getCatOrDog().equals("true")){
            dto.setCatOrDog(true);
        }else{
            dto.setCatOrDog(false);
        }
        dto.setAge(petEntity.getAge());
        dto.setColor(petEntity.getColor());
        if(petEntity.getSex().equals("true")){
            dto.setSex(true);
        }else{
            dto.setSex(false);
        }
        dto.setWeight(petEntity.getWeight());
        dto.setType(petEntity.getType());
        dto.setSubType(petEntity.getSubtype());
        if(petEntity.getOwned().equals("true")){
            dto.setOwned(true);
        }else{
            dto.setOwned(false);
        }
        if(petEntity.getDangerous().equals("true")){
            dto.setDangerous(true);
        }else{
            dto.setDangerous(false);
        }
        if(petEntity.getFound().equals("true")){
            dto.setFound(true);
        }else{
            dto.setFound(false);
        }
        dto.setImage(petEntity.getImage());
        dto.setInterestCounter(petEntity.getInterestCounter());
        if(petEntity.getSuccess().equals("true")){
            dto.setSuccess(true);
        }else{
            dto.setSuccess(false);
        }
        dto.setAnnouncer(AnnouncerMapper.toAnnouncerDTO(petEntity.getHandlerAnnouncer()));
        dto.setUser(UserMapper.toUserDTO(petEntity.getHandlerUser()));
        if(petEntity.getHandlerFoster()!=null){
            dto.setFoster(FosterMapper.toDTO(petEntity.getHandlerFoster()));
        }       
        return dto;

    }
    
    public static List<PetDTO> toPetListDTO(List<PetEntity> petEntities){
    
        List<PetDTO> petDTOList = new ArrayList<>();
        for (int i = 0; i < petEntities.size(); i++) {
            PetDTO dto = PetMapper.toPetDTO(petEntities.get(i));
            petDTOList.add(dto);
        }
    
        return petDTOList;
    
    }
     
    public static List<PetEntity> toPetListEntity(List<PetDTO> petDTOList){
    
        List<PetEntity> petEntityList = new ArrayList<>();
        for (int i = 0; i < petDTOList.size(); i++) {
            PetEntity entity = PetMapper.toPetEntity(petDTOList.get(i));
            petEntityList.add(entity);
        }
        return petEntityList;  
    }
   
}
