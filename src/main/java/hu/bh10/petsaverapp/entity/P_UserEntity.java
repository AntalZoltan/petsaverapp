package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.P_UserEntity.NQ_GET_P_USERS;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "p_user")
@NamedQueries({
    @NamedQuery(name = NQ_GET_P_USERS, query = "select u from P_UserEntity u")
})
public class P_UserEntity {
    
    public static final String NQ_GET_P_USERS = "P_UserEntity.getUsers";
    
    @Id
    @Column(name = "user_email", unique = true, nullable = false)
    private String userEmail;

    @Column(name = "user_last_name", nullable = false)
    private String userLastName;

    @Column(name = "user_first_name", nullable = false)
    private String userFirstName;

    @Column(name = "nick_name", nullable = false)
    private String nickName;

    @Column(name = "user_password", nullable = false)
    private String userPassword;

    @ManyToMany(mappedBy = "userGroups", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserGroup> groups;
    
//    @JoinColumn(name = "user_role")
//    @ManyToOne(fetch = FetchType.EAGER)
//    private UserGroup userRole;

    public P_UserEntity() {
    }

    public P_UserEntity(String userLastName, String userFirstName, String nickName, String userPassword, String userEmail) {
        this.userLastName = userLastName;
        this.userFirstName = userFirstName;
        this.nickName = nickName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String email) {
        this.userEmail = email;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "P_UserEntity{" + "userLastName=" + userLastName + ", userFirstName=" + userFirstName + ", nickName=" + nickName + ", userPassword=" + userPassword + ", userEmail=" + userEmail + ", groups=" + groups + '}';
    }
    

}
