package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.HealthDiagnoseEntity.NQ_GET_DIAGNOSES_BY_ID;
import static hu.bh10.petsaverapp.entity.HealthDiagnoseEntity.PARAM_PET_ID;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "health_diagnose")
@NamedQueries({
    @NamedQuery(name = NQ_GET_DIAGNOSES_BY_ID, query = "select h from HealthDiagnoseEntity h where h.diagnosedPet.id =:"+ PARAM_PET_ID)
})
public class HealthDiagnoseEntity extends BaseEntity {
    
    public static final String NQ_GET_DIAGNOSES_BY_ID = "HealthDiagnoseEntity.getDiagnoses";
    public static final String PARAM_PET_ID = "petId";

    @Column(name = "diag_date", nullable = false)
    private Date diagDate;

    @Column
    private String cause;

    @Column
    private String result;
    
    @Column
    private String medicine;

    @Column(name = "vet_name")
    private String vetName;

    @JoinColumn(name = "pet_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private PetEntity diagnosedPet;


    public Date getDiagDate() {
        return diagDate;
    }

    public void setDiagDate(Date diagDate) {
        this.diagDate = diagDate;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public PetEntity getDiagnosedPet() {
        return diagnosedPet;
    }

    public void setDiagnosedPet(PetEntity diagnosedPet) {
        this.diagnosedPet = diagnosedPet;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public String getVetName() {
        return vetName;
    }

    public void setVetName(String vetName) {
        this.vetName = vetName;
    }

}
